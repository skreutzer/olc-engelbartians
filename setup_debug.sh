#!/bin/sh
# Copyright (C) 2019 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# sudo apt-get install wget unzip make openjdk-8-jdk

OUTLOG="${PWD}/out.log"

java -version >$OUTLOG 2>&1
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip -P ./java/ >>$OUTLOG 2>&1
wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip -P ./java/ >>$OUTLOG 2>&1

cd ./java/ >>$OUTLOG 2>&1
unzip ./digital_publishing_workflow_tools-master.zip >>$OUTLOG 2>&1
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/ >>$OUTLOG 2>&1
cd ./digital_publishing_workflow_tools/ >>$OUTLOG 2>&1
make >>$OUTLOG 2>&1
java -cp ./workflows/setup/setup_1/ setup_1 >>$OUTLOG 2>&1
cd .. >>$OUTLOG 2>&1
unzip ./automated_digital_publishing-master.zip >>$OUTLOG 2>&1
mv ./automated_digital_publishing-master/ ./automated_digital_publishing/ >>$OUTLOG 2>&1
cd ./automated_digital_publishing/ >>$OUTLOG 2>&1
make >>$OUTLOG 2>&1
java -cp ./workflows/ setup1 >>$OUTLOG 2>&1
cd .. >>$OUTLOG 2>&1
cd .. >>$OUTLOG 2>&1
