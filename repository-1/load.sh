#!/bin/sh
# Copyright (C) 2019 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

java -cp ../java/digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/ resource_retriever_1 ./jobfile_resource_retriever_1.xml ./resultinfo_resource_retriever_1.xml
sha256sum ./resources/resource_0 >./resource_hashes.sha256
diff ./resources/hashes.sha256 ./resource_hashes.sha256 >resource_hashes.diff

if [ -s resource_hashes.diff ]
then
    "${EDITOR:-vi}" resource_hashes.diff
    exit 1
else
    rm ./resource_hashes.sha256
    rm ./resource_hashes.diff
fi
